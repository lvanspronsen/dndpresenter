using System;
using Gdk;

namespace DNDPresenter
{
	public static class SceneRenderer
	{
		private static void drawPixbuf(Gdk.Window window, Gdk.GC gc, Pixbuf pixbuf, int x, int y) {
			window.DrawPixbuf(
				gc,
				pixbuf,
				0,
				0,
				x,
				y,
				pixbuf.Width,
				pixbuf.Height,
				RgbDither.None,
				0,
				0);
		}

		public static void RenderScene(Scene scene, bool preview, Gdk.Window window)
		{
			Func<Layer,Pixbuf> getPixbuf = null;
			if (preview) {
				getPixbuf = (l) => l.PreviewPixbuf;
			} else {
				getPixbuf = (l) => l.PresentationPixbuf;
			}

			Func<int,int> scaleX = null;
			Func<int,int> scaleY = null;
			if (preview) {
				scaleX = x => x * scene.PreviewWidth / scene.SceneWidth;
				scaleY = y => y * scene.PreviewHeight / scene.SceneHeight;
			} else {
				scaleX = x => x * scene.PresentationWidth / scene.SceneWidth;
				scaleY = y => y * scene.PresentationHeight / scene.SceneHeight;
			}

			var gc = new Gdk.GC (window);
			var layers = scene.Layers;
			int width, height;
			window.GetSize(out width, out height);

			for(int i = layers.Count - 1; i >= 0; i--) {
				var layer = layers [i];
				var pixbuf = getPixbuf(layer);

				if (!layer.Visible) {
					continue;
				}

				if (layer.Tiled) {
					// TODO: Respect OffsetX / OffsetY here?

					for (int x = 0; x < width; x += pixbuf.Width) {
						for (int y = 0; y < height; y += pixbuf.Height) {
							drawPixbuf(
								window,
								gc,
								pixbuf,
								x,
								y);
						}
					}
				} else {
					drawPixbuf(
						window,
						gc,
						pixbuf,
						scaleX(layer.OffsetX),
						scaleY(layer.OffsetY));
				}
			}

			if (scene.FogOfWar) {

				Cairo.Region r = new Cairo.Region ();

				r.UnionRectangle(new Cairo.RectangleInt () {
					X = 0,
					Y = 0,
					Width = width,
					Height = height
				});

				foreach(FogRegion fr in scene.FogRegions) {
					var rect = new Cairo.RectangleInt () {
						X = (int)(fr.X * width),
						Y = (int)(fr.Y * height),
						Width = (int)(fr.Width * width),
						Height = (int)(fr.Height * height)
					};

					if (fr.Include) {
						r.UnionRectangle(rect);
					} else {
						r.SubtractRectangle(rect);
					}
				}


				using(var context = Gdk.CairoHelper.Create(window)) {
					if (preview) {
						context.SetSourceRGBA(0, 0, 0, .5);
					} else {
						context.SetSourceRGBA(0, 0, 0, 1.0);
					}

					for(int i = 0; i < r.NumRectangles; i++) {
						var rect = r.GetRectangle(i);
						var rect2 = new Cairo.Rectangle (
							            rect.X,
							            rect.Y,
							            rect.Width,
							            rect.Height);

						context.Rectangle(rect2);
						context.Fill();
					}
				}
			}
		}
	}
}

