using System;
using Gtk;

namespace DNDPresenter
{
	public partial class PresentationWindow : Gtk.Window
	{
		public PresentationWindow () : base(Gtk.WindowType.Toplevel)
		{
			Build();
			this.drawingArea.ExposeEvent += this.areaExposed;
			Scene.Current.Changed += this.sceneChanged;
		}

		protected void OnDeleteEvent (object sender, DeleteEventArgs a)
		{
			Application.Quit();
			a.RetVal = true;
		}

		protected void sceneChanged() {
			this.drawingArea.QueueDraw();
		}

		protected void areaExposed(object s, ExposeEventArgs e) {
			SceneRenderer.RenderScene(Scene.Current, false, drawingArea.GdkWindow);
		}
	}
}

