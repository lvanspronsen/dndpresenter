using System;
using System.Collections.Generic;

namespace DNDPresenter
{
	public class Scene
	{
		public static readonly Scene Current = new Scene();

		private int sceneWidth, sceneHeight;
		private int presentationWidth, presentationHeight;
		private int previewWidth, previewHeight;
		private List<Layer> layers;
		private bool fogOfWar;
		private List<FogRegion> fogRegions;
		public event Action Changed;

		public int SceneWidth { get { return sceneWidth; } }
		public int SceneHeight { get { return sceneHeight; } }
		public int PresentationWidth { get { return presentationWidth; } }
		public int PresentationHeight { get { return presentationHeight; } }
		public int PreviewWidth { get { return previewWidth; } }
		public int PreviewHeight { get { return previewHeight; } }

		public List<Layer> Layers { get { return layers; } }
		public bool FogOfWar { get { return fogOfWar; } set { fogOfWar = value; } }
		public List<FogRegion> FogRegions { get { return fogRegions; } }


		public Scene ()
		{
			this.sceneWidth = 1920;
			this.sceneHeight = 1080;
			this.presentationWidth = 1920;
			this.presentationHeight = 1080;
			this.previewWidth = 1280;
			this.previewHeight = 720;

			this.layers = new List<Layer> ();
			this.fogOfWar = false;
			this.fogRegions = new List<FogRegion> ();
		}

		public void SetSceneSize(int width, int height) {
			this.sceneWidth = width;
			this.sceneHeight = height;
			foreach (var layer in layers) {
				layer.PresentationSizeChanged();
				layer.PreviewSizeChanged();
			}
			this.OnChanged();
		}

		public void SetPresentationSize(int width, int height) {
			this.presentationWidth = width;
			this.presentationHeight = height;
			foreach (var layer in layers) {
				layer.PresentationSizeChanged();
			}
			this.OnChanged();
		}

		public void SetPreviewSize(int width, int height) {
			this.previewWidth = width;
			this.previewHeight = height;
			foreach (var layer in layers) {
				layer.PreviewSizeChanged();
			}
			this.OnChanged();
		}

		public void AddLayer(Layer l) {
			this.layers.Add(l);
			this.OnChanged();
		}

		public void AddFogRegion(FogRegion f) {
			this.fogRegions.Add(f);
			this.OnChanged();
		}

		public void OnChanged() {
			var changed = this.Changed;
			if (changed != null) {
				changed();
			}
		}
	}
}

