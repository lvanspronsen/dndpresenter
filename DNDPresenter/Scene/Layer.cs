using System;
using Gdk;

namespace DNDPresenter
{
	public class Layer
	{
		protected Scene scene { get { return Scene.Current; } }

		private string filename;
		private string path;
		private Pixbuf pixbuf;
		private Pixbuf presentationPixbuf;
		private Pixbuf previewPixbuf;
		private bool tiled;
		private bool visible;
		private int offsetX;
		private int offsetY;

		public string FileName { get { return filename; } }
		public string Path { get { return path; } }
		public bool Tiled { get { return tiled; } set { tiled = value; } }
		public bool Visible { get { return visible; } set { visible = value; } }
		public Pixbuf Pixbuf { get { return pixbuf; } }
		public Pixbuf PresentationPixbuf { get { return presentationPixbuf; } }
		public Pixbuf PreviewPixbuf { get { return previewPixbuf; } }
		public int OffsetX { get { return offsetX; } set { offsetX = value; } }
		public int OffsetY { get { return offsetY; } set { offsetY = value; } }

		public Layer (string filename, string path)
		{
			this.filename = filename;
			this.path = path;
			this.tiled = false;
			this.visible = true;
			this.offsetX = 0;
			this.offsetY = 0;

			this.pixbuf = new Pixbuf (path);
			this.PresentationSizeChanged();
			this.PreviewSizeChanged();
		}

		public void PresentationSizeChanged()
		{
			this.presentationPixbuf = this.pixbuf.ScaleSimple(
				this.pixbuf.Width * scene.PresentationWidth / scene.SceneWidth,
				this.pixbuf.Height * scene.PresentationHeight / scene.SceneHeight,
				InterpType.Bilinear);
		}

		public void PreviewSizeChanged()
		{
			this.previewPixbuf = this.pixbuf.ScaleSimple(
				this.pixbuf.Width * scene.PreviewWidth / scene.SceneWidth,
				this.pixbuf.Height * scene.PreviewHeight / scene.SceneHeight,
				InterpType.Bilinear);
		}
	}
}

