using System;

namespace DNDPresenter
{
	public class FogRegion
	{
		public bool Include;
		public double X;
		public double Y;
		public double Width;
		public double Height;

		public FogRegion ()
		{
		}
	}
}

