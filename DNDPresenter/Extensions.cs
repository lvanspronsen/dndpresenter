using System;

namespace DNDPresenter
{
	public static class Extensions
	{
		public static Cairo.Rectangle ToCairoRect(this Gdk.Rectangle rect) {
			return new Cairo.Rectangle (
				(double)rect.X,
				(double)rect.Y,
				(double)rect.Width,
				(double)rect.Height);
		}

		public static Gdk.Rectangle ToGdkRect(this Cairo.Rectangle rect) {
			return new Gdk.Rectangle (
				(int)rect.X,
				(int)rect.Y,
				(int)rect.Width,
				(int)rect.Height);
		}
	}
}

