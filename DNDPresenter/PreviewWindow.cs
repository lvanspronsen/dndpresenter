using System;
using Gtk;

namespace DNDPresenter
{
	public partial class PreviewWindow: Gtk.Window
	{
		private bool dragging;
		private double dragStartX;
		private double dragStartY;
		private double dragStopX;
		private double dragStopY;

		public PreviewWindow () : base(Gtk.WindowType.Toplevel)
		{
			Build();

			this.AddEvents(
				(int)Gdk.EventMask.AllEventsMask);

			this.drawingArea.AddEvents(
				(int)Gdk.EventMask.ButtonPressMask |
				(int)Gdk.EventMask.ButtonReleaseMask |
				(int)Gdk.EventMask.Button1MotionMask);

			this.drawingArea.ExposeEvent += this.areaExposed;
			Scene.Current.Changed += this.sceneChanged;
		}

		protected void OnSizeAllocated(object sender, SizeAllocatedArgs e)
		{
			Scene.Current.SetPreviewSize(e.Allocation.Width, e.Allocation.Height);
		}

		protected void OnDeleteEvent (object sender, DeleteEventArgs a)
		{
			Application.Quit();
			a.RetVal = true;
		}

		protected Cairo.Rectangle dragRectangle() {
			double x = dragStartX;
			double width = dragStopX - dragStartX;
			if (width < 0) {
				x = dragStopX;
				width = dragStartX - dragStopX;
			}

			double y = dragStartY;
			double height = dragStopY - dragStartY;
			if (height < 0) {
				y = dragStopY;
				height = dragStartY - dragStopY;
			}

			return new Cairo.Rectangle (
				x,
				y,
				width,
				height);
		}

		protected void sceneChanged() {
			this.drawingArea.QueueDraw();
		}

		protected void areaExposed(object s, ExposeEventArgs e) {
			var scene = Scene.Current;
			SceneRenderer.RenderScene(scene, true, drawingArea.GdkWindow);

			if (scene.FogOfWar && dragging) {

				Gdk.GC rectGC = new Gdk.GC (drawingArea.GdkWindow);
				rectGC.SetLineAttributes(
					2,
					Gdk.LineStyle.OnOffDash,
					Gdk.CapStyle.Butt,
					Gdk.JoinStyle.Miter);

				var rect = dragRectangle().ToGdkRect();

				drawingArea.GdkWindow.DrawRectangle(
					rectGC,
					false,
					rect);
			}
		}

		protected void OnDrawingAreaButtonPressEvent (object o, ButtonPressEventArgs args)
		{
			var scene = Scene.Current;
			var ev = args.Event;
			if (scene.FogOfWar && ev.Device.HasCursor && ev.Button == 1) {
				dragging = true;
				dragStartX = ev.X;
				dragStartY = ev.Y;
			}
		}

		protected void OnDrawingAreaButtonReleaseEvent (object o, ButtonReleaseEventArgs args)
		{
			var scene = Scene.Current;
			var ev = args.Event;
			if (scene.FogOfWar && ev.Device.HasCursor && ev.Button == 1 && dragging) {
				dragging = false;

				int width;
				int height;
				this.GetSize(out width, out height);
				var rect = dragRectangle();

				var region = new FogRegion () {
					X = rect.X / (double)width,
					Y = rect.Y / (double)height,
					Width = rect.Width / (double)width,
					Height = rect.Height / (double)height,
					Include = (ev.State & Gdk.ModifierType.ShiftMask) != Gdk.ModifierType.None
				};

				scene.FogRegions.Add(region);
				scene.OnChanged();
			}
		}

		protected void OnDrawingAreaMotionNotifyEvent (object o, MotionNotifyEventArgs args)
		{
			var ev = args.Event;
			if (ev.Device.HasCursor && dragging) {
				dragStopX = ev.X;
				dragStopY = ev.Y;
				this.QueueDraw();
			}
		}
	}
}
