using System;
using Gtk;

namespace DNDPresenter
{
	public class MainClass
	{
		public static LayersWindow Layers;
		public static PreviewWindow Preview;
		public static PresenterWindow Presenter;

		public static void Main (string[] args)
		{
			Application.Init();

			Layers = new LayersWindow ();
			Preview = new PreviewWindow ();
			Presenter = new PresenterWindow ();

			Layers.Show();
			Preview.Show();
			Presenter.Show();

			Application.Run();
		}
	}
}
