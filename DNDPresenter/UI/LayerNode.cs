using System;
using Gtk;

namespace DNDPresenter
{
	[Gtk.TreeNode(ListOnly=true)]
	public class LayerNode : Gtk.TreeNode
	{
		public int Index { get; set; }

		[Gtk.TreeNodeValue(Column=0)]
		public string FileName { get; set; }

		[Gtk.TreeNodeValue(Column=1)]
		public bool Tiled{ get; set; }

		[Gtk.TreeNodeValue(Column=2)]
		public bool Visible { get; set; }

		public LayerNode ()
		{
		}

	}
}

