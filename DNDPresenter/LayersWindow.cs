using System;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using Gtk;
using ICSharpCode.SharpZipLib.Zip;
using IOPath = System.IO.Path;

namespace DNDPresenter
{
	public partial class LayersWindow : Gtk.Window
	{
		private NodeStore store;
		private int selectedIndex = -1;

		public LayersWindow () : 
			base(Gtk.WindowType.Toplevel)
		{
			this.Build();
			layersNodeView.AppendColumn("Image", new Gtk.CellRendererText (), "text", 0);
			layersNodeView.AppendColumn("Tiled", new Gtk.CellRendererText (), "text", 1);
			layersNodeView.AppendColumn("Visible", new Gtk.CellRendererText (), "text", 2);

			sceneWidthSB.Value = (int)Scene.Current.SceneWidth;
			sceneHeightSB.Value = (int)Scene.Current.SceneHeight;
			Scene.Current.Changed += this.sceneChanged;
		}

		protected void sceneChanged() {
			store = new NodeStore (typeof(LayerNode));
			var layers = Scene.Current.Layers;

			if (selectedIndex == -1) {
				var selectedNode = getSelectedNode();
				if (selectedNode != null) {
					selectedIndex = selectedNode.Index;
				}
			}

			for (var i = 0; i < layers.Count; i++) {
				var layer = layers [i];
				LayerNode node = new LayerNode ();
				node.Index = i;
				node.FileName = layer.FileName;
				node.Tiled = layer.Tiled;
				node.Visible = layer.Visible;
				store.AddNode(node);
			}
			layersNodeView.NodeStore = store;

			if (selectedIndex != -1) {
				layersNodeView.Selection.Mode = SelectionMode.Single;
				layersNodeView.Selection.SelectPath(
					new TreePath (new int[] { selectedIndex }));
				selectedIndex = -1;
			}
		}

		protected void OnSetSceneSizeBtnClicked (object sender, EventArgs e)
		{
			int sceneWidth = sceneWidthSB.ValueAsInt;
			int sceneHeight = sceneHeightSB.ValueAsInt;
			Scene.Current.SetSceneSize(sceneWidth, sceneHeight);
		}

		private void addOpenRasterLayers(string filename) {
			var tempDir = IOPath.GetTempPath();
			FastZip fz = new FastZip ();
			fz.ExtractZip(filename, tempDir, null);

			var stackPath = IOPath.Combine(tempDir, "stack.xml");
			XElement rootEl = XElement.Load(stackPath);
			var els = rootEl.Element("stack").Elements("layer");
			Console.WriteLine(rootEl.ToString());
			foreach (var el in els) {
				var name = el.Attribute("name").Value;
				var src = el.Attribute("src").Value;
				var x = el.Attribute("x");
				var y = el.Attribute("y");

				Layer layer = new Layer (name, IOPath.Combine(tempDir, src));
				if (x != null) {
					layer.OffsetX = Convert.ToInt32(x.Value);
				}
				if (y != null) {
					layer.OffsetY = Convert.ToInt32(y.Value);
				}

				Scene.Current.AddLayer(layer);
			}
		}

		protected void OnAddLayerBtnClicked (object sender, EventArgs e)
		{
			FileChooserDialog dialog = new FileChooserDialog (
				                           "Choose Layer Image",
				                           this,
				                           FileChooserAction.Open,
				                           "Open", ResponseType.Accept,
				                           "Cancel", ResponseType.Cancel);



			FileFilter filter = new FileFilter ();
			filter.Name = "Images";
			filter.AddPattern("*.gif");
			filter.AddPattern("*.png");
			filter.AddPattern("*.jpg");
			filter.AddPattern("*.jpeg");
			filter.AddPattern("*.ora");
			dialog.AddFilter(filter);


			int response = dialog.Run();
			if (response == (int)ResponseType.Accept) {
				string path = dialog.Filename;
				string filename = System.IO.Path.GetFileName(path);
				if (filename.EndsWith(".ora")) {
					// open raster file, so we need to add multiple
					// layers
					addOpenRasterLayers(path);
				} else {
					Layer layer = new Layer (filename, path);
					Scene.Current.AddLayer(layer);
				}
			}

			dialog.Destroy();

		}

		private LayerNode getSelectedNode() {
			var selection = layersNodeView.NodeSelection;
			var node = selection.SelectedNode as LayerNode;
			if (selection.Mode == SelectionMode.Single && node != null) {
				return node;
			} else {
				return null;
			}
		}

		protected void OnUpLayerBtnClicked (object sender, EventArgs e)
		{
			var scene = Scene.Current;
			var layers = scene.Layers;
			var node = getSelectedNode();

			if (node != null && node.Index > 0) {
				int index = node.Index;
				var temp = layers [index - 1];
				layers [index - 1] = layers [index];
				layers [index] = temp;
				selectedIndex = index - 1;
				scene.OnChanged();
			}
		}

		protected void OnDownLayerBtnClicked (object sender, EventArgs e)
		{
			var scene = Scene.Current;
			var layers = scene.Layers;
			var node = getSelectedNode();

			if (node != null && node.Index < layers.Count - 1) {
				int index = node.Index;
				var temp = layers [index + 1];
				layers [index + 1] = layers [index];
				layers [index] = temp;
				selectedIndex = index + 1;
				scene.OnChanged();
			}
		}

		protected void OnToggleTiledBtnClicked (object sender, EventArgs e)
		{
			var scene = Scene.Current;
			var node = getSelectedNode();

			if (node != null) {
				var layer = scene.Layers [node.Index];
				layer.Tiled = !layer.Tiled;
				scene.OnChanged();
			}
		}

		protected void OnToggleVisibleBtnClicked (object sender, EventArgs e)
		{
			var scene = Scene.Current;
			var node = getSelectedNode();

			if (node != null) {
				var layer = scene.Layers [node.Index];
				layer.Visible = !layer.Visible;
				scene.OnChanged();
			}
		}

		protected void OnFogToggleBtnClicked (object sender, EventArgs e)
		{
			Scene.Current.FogOfWar = !Scene.Current.FogOfWar;
			Scene.Current.OnChanged();
		}

		protected void OnToggleDecorateBtnClicked (object sender, EventArgs e)
		{
			MainClass.Presenter.Decorated = !MainClass.Presenter.Decorated;
			if (!MainClass.Presenter.Decorated) {
				MainClass.Presenter.Maximize();
			} else {
				MainClass.Presenter.Unmaximize();
			}
		}

	}
}

